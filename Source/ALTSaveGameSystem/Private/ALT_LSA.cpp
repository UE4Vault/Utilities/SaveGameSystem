// Fill out your copyright notice in the Description page of Project Settings.

#include "ALT_LSA.h"

DEFINE_LOG_CATEGORY(LogLevelScriptActor);

/*
void AALT_LSA::AddHUDToScreen(TSubclassOf<UUserWidget> W_HUD)
{
	if (W_HUD) // Check if the Asset is assigned in the blueprint.
	{
		// Create the widget and store it.
		HUD = CreateWidget<UUserWidget>(UGameplayStatics::GetPlayerController(GetWorld(), 0), W_HUD);

		// now you can use the widget directly since you have a referance for it.
		// Extra check to  make sure the pointer holds the widget.
		if (HUD)
		{
			//lets add it to the view port
			HUD->AddToViewport();
		}

		//Show the Cursor.
		//UGameplayStatics::GetPlayerController(GetWorld(), 0)->bShowMouseCursor = true;
	}
}*/

void AALT_LSA::LoadAndSetCharacterPosition(FString SaveName, bool SetToPlayerStart)
{
	UALT_SG* SaveGameInstance = UALT_FL::SGBase(SaveName, SaveName);
	//If SaveName is valid continue, else show error message.
	if (!SaveName.Equals("") && SaveGameInstance->IsValidLowLevel())
	{
		//If saved transform equals 0,0,0 and SetToPlayerStart equals true.
		if (SaveGameInstance->Transform.GetLocation().Equals(FVector(0, 0, 0), .000100) && SetToPlayerStart == true)
		{
			for (TActorIterator<ACheckpoint> ActorItr(GetWorld()); ActorItr; ++ActorItr)
			{
				// Same as with the Object Iterator, access the subclass instance with the * or -> operators.
				APlayerStart* PlayerStart = *ActorItr;
				SaveGameInstance->Transform = PlayerStart->GetActorTransform();
			}
			UGameplayStatics::SaveGameToSlot(SaveGameInstance, SaveName, 0);
			UGameplayStatics::GetPlayerCharacter(GetWorld(), 0)->SetActorTransform(SaveGameInstance->Transform, false, nullptr, ETeleportType::TeleportPhysics);
		}
		//
		else if (!SaveGameInstance->Transform.GetLocation().Equals(FVector(0, 0, 0), .000100))
		{
			UGameplayStatics::GetPlayerCharacter(GetWorld(), 0)->SetActorTransform(SaveGameInstance->Transform, false, nullptr, ETeleportType::TeleportPhysics);
		}
	}
	else
	{
		UE_LOG(LogLevelScriptActor, Warning, TEXT("SaveGame instance is null"));
	}
}
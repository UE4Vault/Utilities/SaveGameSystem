// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/SaveGame.h"
#include "UStructLibrary.h"
#include "UEnumLibrary.h"
#include "ALT_SG.generated.h"

/**
 * 
 */
UCLASS()
class ALTSAVEGAMESYSTEM_API UALT_SG : public USaveGame
{
	GENERATED_BODY()
public:
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Options Variable")
		bool FullscreenCheckbox = true;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Options Variable")
		bool DeveloperCheckbox = true;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Options Variable")
		float QualityValue = 1;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Options Variable")
		float InvertX = 2;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Options Variable")
		float InvertY = -2;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Options Variable")
		float MouseXSensitivity = .5;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Options Variable")
		float MouseYSensitivity = .5;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Options Variable")
		FString ResolutionSelection = "";
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Character Variable")
		FTransform Transform;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Character Variable")
		float Health = 100;
//	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Structures")
//		TArray<FKey> KeybindInputs;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Structures")
		FLevelStruct LevelStruct;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Structures")
		FSoundStruct SoundStruct;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Checkpoint")
		TArray<int32> DoorsOpened;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Checkpoint")
		TArray<FSavedItem> SavedItems;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Checkpoint")
		TArray<int32> SpawnersActivated;
//	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Checkpoint")
//		TArray<FNPCActor> NPCActors;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Inventory")
		int32 Gold;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Inventory")
		TArray<FInventorySlot> Slots;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Inventory")
		int32 AmountOfSlots;
};

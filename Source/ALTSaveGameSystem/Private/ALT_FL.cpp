// Fill out your copyright notice in the Description page of Project Settings.

#include "ALT_FL.h"

UALT_SG* UALT_FL::SGBase(FString SaveName, FString& SaveNameReturn)
{
	SaveNameReturn = SaveName;
	UALT_SG* SaveGameInstance;
	//Check if save exists and if false make a new one
	if (UGameplayStatics::DoesSaveGameExist(SaveName, 0) == false)
	{
		SaveGameInstance = Cast<UALT_SG>(UGameplayStatics::CreateSaveGameObject(UALT_SG::StaticClass()));
		UGameplayStatics::SaveGameToSlot(SaveGameInstance, SaveName, 0);
	}
	//Load game cast reference into variable
	SaveGameInstance = Cast<UALT_SG>(UGameplayStatics::LoadGameFromSlot(SaveName, 0));
	//If reference is valid return it, otherwise return nullptr and error message
	if (SaveGameInstance->IsValidLowLevel())
	{
		return SaveGameInstance;
	}
	else
	{
		UE_LOG(LogTemp, Error, TEXT("SGSaveGame not valid"))
		return nullptr;
	}
}

UALT_SG* UALT_FL::SGMBase(FString& SaveNameReturn)
{
	FString SaveName = "Settings";
	SaveNameReturn = SaveName;
	//Cast to SaveGame using SaveName
	UALT_SG* SaveGameInstance = Cast<UALT_SG>(SGBase(SaveName, SaveName));
	//If Cast is valid, return Cast otherwise return nullptr and give error message
	if (SaveGameInstance->IsValidLowLevel())
	{
		return SaveGameInstance;
	}
	else
	{
		UE_LOG(LogTemp, Log, TEXT("SGMBase Failed"));
		return nullptr;
	}
}

UALT_SG* UALT_FL::SGMPure()
{
	FString SaveNameReturn = "";
	return SGMBase(SaveNameReturn);
}

TArray<FString> UALT_FL::GetAllSaveGameSlotNames()
{
	TArray<FString> ret;
	ISaveGameSystem* SaveSystem = IPlatformFeaturesModule::Get().GetSaveGameSystem();

	// If we have a save system and a valid name..
	if (SaveSystem)
	{
		// From SaveGameSystem.h in the Unreal source code base.
		FString saveGamePath = FString::Printf(TEXT("%s/SaveGames/*"), *FPaths::ProjectSavedDir());

		GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, TEXT("Search path %s"), *saveGamePath);

		WIN32_FIND_DATA fd;
		HANDLE hFind = ::FindFirstFile(*saveGamePath, &fd);
		if (hFind != INVALID_HANDLE_VALUE) {
			do {
				GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, TEXT("  Save Name: %s"), fd.cFileName);
				// Disallow empty names and . and .. and don't get all tied up in WCHAR issues.
				if (fd.cFileName[0] == '\0' ||
					fd.cFileName[0] == '.' && fd.cFileName[1] == '\0' ||
					fd.cFileName[0] == '.' && fd.cFileName[1] == '.' && fd.cFileName[2] == '\0')
				{
				}
				//Check if: This is a directory && The first letter of the file name contains a "." && File name contains .sav && Is not the MainMenu.sav
				else if ((FILE_ATTRIBUTE_DIRECTORY) != 0 && fd.cFileName[0] != '.' && FString(fd.cFileName).Contains(".sav") == true && FString(fd.cFileName).Equals("MainMenu.sav") == false) {
					GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, TEXT("Adding File!"));
					ret.Add(FString(fd.cFileName).Replace(TEXT(".sav"), TEXT("")));
				}
			} while (::FindNextFile(hFind, &fd));
			::FindClose(hFind);
		}
	}

	return ret;
}

TArray<FString> UALT_FL::GetAllLevelNames()
{
	TArray<FString> ret;
	FString ContentPath = FString::Printf(TEXT("%s/*"), *FPaths::ProjectContentDir());

	WIN32_FIND_DATA fd;
	HANDLE hFind = ::FindFirstFile(*ContentPath, &fd);

	if (hFind != INVALID_HANDLE_VALUE)
	{
		do
		{
			// Disallow empty names and . and .. and don't get all tied up in WCHAR issues.
			if (fd.cFileName[0] == '\0' ||
				fd.cFileName[0] == '.' && fd.cFileName[1] == '\0' ||
				fd.cFileName[0] == '.' && fd.cFileName[1] == '.' && fd.cFileName[2] == '\0')
			{

			}
			//Check if: This is a directory && The first letter of the file name contains a "." && File name contains .umap
			else if ((FILE_ATTRIBUTE_DIRECTORY) != 0 && fd.cFileName[0] != '.' && FString(fd.cFileName).Contains(".umap")) {
				ret.Add(FString(fd.cFileName).Replace(TEXT(".umap"), TEXT("")));
			}
		} while (::FindNextFile(hFind, &fd));
		::FindClose(hFind);
	}

	return ret;
}

bool UALT_FL::FileSaveString(FString SaveText, FString FileName)
{
	return FFileHelper::SaveStringToFile(SaveText, *(FPaths::ProjectDir() + FileName));
}

bool UALT_FL::FileLoadString(FString FileName, FString& SaveText)
{
	return FFileHelper::LoadFileToString(SaveText, *(FPaths::ProjectDir() + FileName));
}

void UALT_FL::SetLevelAndLoad(TSoftObjectPtr<UWorld> Level)
{
	FString SaveName;

	UALT_SG* LSave = SGBase("SGUser", SaveName);

	LSave->LevelStruct.Level = FName(*Level.GetAssetName());

	UGameplayStatics::SaveGameToSlot(LSave, SaveName, 0);

	UEngine* LGEngine = GEngine;
	if (LGEngine == NULL || LGEngine->GameViewport == NULL)
		return;

	UGameplayStatics::OpenLevel(LGEngine->GameViewport->GetWorld(), FName(*Level.GetAssetName()));
}

int32 UALT_FL::ConcatInt(int32 A, int32 B)
{
	int32 pow = 10;
	while (B >= pow)
		pow *= 10;
	return A * pow + B;
}

bool UALT_FL::BooleanXNOR(bool A, bool B)
{
	return !(A ^ B);
}

float UALT_FL::MakeOperatorsTheSame(float OperatorToChange, float OperatorBase)
{
	return BooleanXNOR(OperatorToChange < 0, OperatorBase < 0) ? OperatorToChange : OperatorToChange * -1;
}

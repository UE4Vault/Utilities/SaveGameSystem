#pragma once

#include "UEnumLibrary.h"
#include "Engine/DataTable.h"
#include "GameFramework/Actor.h"
#include "UStructLibrary.generated.h"

USTRUCT(BlueprintType)
struct FAchievementStruct
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Achievement Structure")
		float Percentage;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Achievement Structure")
		bool Completed;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Achievement Structure")
		TArray<bool> AchievementList;

	FAchievementStruct()
	{
		Percentage = 0;
		Completed = false;
	}
};

USTRUCT(BlueprintType)
struct FLevelStruct
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Level Structure")
		int32 Difficulty;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Level Structure")
		FName Level;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Level Structure")
		TArray<int32> PassedCheckpoints;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Level Structure")
		TArray<FAchievementStruct> AchievementStruct;

	FLevelStruct()
	{
		Difficulty = 1;
		Level = "Level1";
	}
};

USTRUCT(BlueprintType)
struct FSoundStruct
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Sound Structure")
		TArray<FString> SoundType;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Sound Structure")
		TArray<float> Volume;

	FSoundStruct()
	{
		FString SoundTypeInit[] = {"Master", "Music", "Ambient", "UI", "Dialog"};
		SoundType.Append(SoundTypeInit, ARRAY_COUNT(SoundTypeInit));
		float VolumeInit[] = {1, .5 ,.5 ,.5 ,.5};
		Volume.Append(VolumeInit, ARRAY_COUNT(VolumeInit));
	}
};

USTRUCT(BlueprintType)
struct FAchieveableStruct : public FTableRowBase
{
	GENERATED_BODY()

public:

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Achieveable Structure")
		int32 AchieveableList;

	FAchieveableStruct()
	{
		AchieveableList = 0;
	}
};

USTRUCT(BlueprintType)
struct FSavedItem
{
	GENERATED_BODY()

	/*The ID of this item to set apart from others of the same class when saving*/
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Inventory")
		int32 ID;

	/*The leftover stack amount for this item after some has been added to the inventory*/
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Inventory")
		int32 Rest;

	FSavedItem()
	{
		ID = 0;
		Rest = 0;
	}
};

USTRUCT(BlueprintType)
struct FItemInformation : public FTableRowBase
{
	GENERATED_BODY()

		/*The name of this item*/
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Inventory")
		FText ItemName;

	/*The description of this item*/
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Inventory")
		FText ItemDescription;

	/*The value of this item*/
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Inventory")
		int32 Value;

	/*The image shown in the inventory*/
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Inventory")
		UTexture2D* Thumbnail;

	/*The category that this item is in*/
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Inventory")
		EItemCategories Category;

	/*Can this item be used*/
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Inventory")
		bool CanBeUsed;

	/*Can this item be stacked*/
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Inventory")
		bool CanBeStacked;

	FItemInformation()
	{
		ItemName = NSLOCTEXT("Evolution", "Evoltion", "Item Name");
		ItemDescription = NSLOCTEXT("Evolution", "Evoltion", "Item Description");
		Value = 0;
		Thumbnail = NULL;
		Category = EItemCategories::IC_Consumable;
		CanBeUsed = true;
		CanBeStacked = false;
	}
};

USTRUCT(BlueprintType)
struct FInventorySlot
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Inventory")
		TSubclassOf<AActor> ItemClass;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Inventory")
		int32 StackAmount;

	FInventorySlot()
	{
		ItemClass = AActor::StaticClass();
		StackAmount = 1;
	}
};

USTRUCT(BlueprintType)
struct FStatusEffect
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "DamageType")
		EStatusEffect StatusEffect;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "DamageType")
		int32 Strength;

	FStatusEffect()
	{
		StatusEffect = EStatusEffect::SE_Poison;
		Strength = 1;
	}
};

USTRUCT(BlueprintType)
struct FDialog
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Dialog")
		FString Dialog;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Dialog")
		TArray<FString> Answers;

	FDialog()
	{
		Dialog = "";
	}
};

USTRUCT(BlueprintType)
struct FNPCActor
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Checkpoint")
		FString Level;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Checkpoint")
		FTransform Transform;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Checkpoint")
		float Health;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Checkpoint")
		FText DefaultHelpText;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Checkpoint")
		TArray<FDialog> DialogAndAnswers;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Checkpoint")
		TArray<FInventorySlot> Offer;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Checkpoint")
		float InteractRange;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Checkpoint")
		bool ShowHelpText;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Checkpoint")
		FVector HelpTextLocation;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Checkpoint")
		float HelpTextFontSize;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Checkpoint")
		USoundBase* ShopOpeningSound;

	FNPCActor()
	{
		Level = "";
		Transform = FTransform::Identity;
		Health = 0;
		DefaultHelpText = NSLOCTEXT("Evolution", "Evoltion", "");
		InteractRange = 0;
		ShowHelpText = false;
		HelpTextLocation = FVector::ZeroVector;
		HelpTextFontSize = 0;
		ShopOpeningSound = NULL;
	}
};
// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/LevelScriptActor.h"
#include "ALT_SG.h"
#include "ALT_FL.h"
#include "Checkpoint.h"
#include "EngineUtils.h"
#include "UserWidget.h"
#include "Engine/World.h"
#include "ALT_LSA.generated.h"

DECLARE_LOG_CATEGORY_EXTERN(LogLevelScriptActor, Log, All);


UCLASS()
class ALTSAVEGAMESYSTEM_API AALT_LSA : public ALevelScriptActor
{
	GENERATED_BODY()
public:


	// FUNCTIONS


//	UFUNCTION(BlueprintCallable, Category = "Screen")
//		void AddHUDToScreen(TSubclassOf<UUserWidget> W_HUD);

	UFUNCTION(BlueprintCallable, Category = "Player")
		void LoadAndSetCharacterPosition(FString SaveName, bool SetToPlayerStart);


	// PROPERTIES


	/*Is this level meant for gameplay | True by default*/
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Level")
		bool bGameplayLevel = true;

	/*Should we save the current state of this level*/
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Level")
		bool bSaveLevel;

	/*Variable to hold the widget after creating it*/
//	UPROPERTY(BlueprintReadOnly)
//		UUserWidget* HUD;


};

// Fill out your copyright notice in the Description page of Project Settings.

#include "Checkpoint.h"

DEFINE_LOG_CATEGORY(LogCheckpoint);

ACheckpoint::ACheckpoint(const FObjectInitializer& ObjectInitializer) : APlayerStart(ObjectInitializer)
{
	PrimaryActorTick.bCanEverTick = false;

	Trigger = CreateDefaultSubobject<UBoxComponent>(FName("TriggerBox"));
	if (Trigger)
	{
		Trigger->SetupAttachment(RootComponent);
		Trigger->RegisterComponentWithWorld(GetWorld());
		Trigger->bHiddenInGame = false;
		Trigger->SetBoxExtent(FVector(64, 64, 300));
		Trigger->OnComponentBeginOverlap.AddDynamic(this, &ACheckpoint::OnOverlapBegin);
	}

	GetCapsuleComponent()->SetMobility(EComponentMobility::Movable);

	Distances.Init(FVector(0, 0, 0), 4);
	Locations.Init(FVector(0, 0, 0), 2);
}

void ACheckpoint::SetActorCenter(AActor* Actor, TArray<FVector> Distances, TArray<FVector> Locations)
{
	//Use distances to find the center of the surroundings
	float A = (UKismetMathLibrary::Abs(Distances[0].X) + UKismetMathLibrary::Abs(Distances[1].X)) / 2;
	float B = (UKismetMathLibrary::Abs(Distances[2].Y) + UKismetMathLibrary::Abs(Distances[3].Y)) / 2;

	//Same Signs
//	float SSA = !((A < 0) ^ (Locations[0].X < 0)) ? A : A * -1;

	//Same Signs
//	float SSB = !((B < 0) ^ (Locations[1].Y < 0)) ? B : B * -1;


	//Make the operators the same as the plane we're calculating at
	float SSA = UALT_FL::MakeOperatorsTheSame(A, Locations[0].X);
	float SSB = UALT_FL::MakeOperatorsTheSame(B, Locations[1].Y);

	if (Actor)
		Actor->SetActorLocation(FVector((SSA - Locations[0].X) * -1, (SSB - Locations[1].Y) * -1, Locations[0].Z)); //Negative 1 on Y side might be the cause of Checkpoint centering too far on -Y
}

void ACheckpoint::SetBoxSize(UBoxComponent* BoxComponent)
{
	//Object Axis for Loop
	for (int i = 0; i <= 3; i++)
	{
		FVector Select = i == 0 ? Trigger->GetForwardVector() * -1 : Trigger->GetForwardVector();
		Select = i == 2 ? Trigger->GetRightVector() * -1 : Select;
		Select = i == 3 ? Trigger->GetRightVector() : Select;

		FVector CurrentAxis = Select;
		FHitResult Hit;

		GetWorld()->LineTraceSingleByObjectType(Hit, GetActorLocation(), GetActorLocation() + CurrentAxis * Distance, FCollisionObjectQueryParams(ECollisionChannel::ECC_WorldStatic));

		if (CurrentAxis.X == -1 || CurrentAxis.Y == -1)
		{
			BoxSize = Hit.Location - Hit.TraceStart + BoxSize;

			//Set Box Extent
			Trigger->SetBoxExtent(FVector(UKismetMathLibrary::Abs(BoxSize.X), UKismetMathLibrary::Abs(BoxSize.Y), Trigger->GetScaledBoxExtent().Z));
//			UE_LOG(LogCheckpoint, Log, TEXT("Box Size: %s"), *FVector(UKismetMathLibrary::Abs(BoxSize.X), UKismetMathLibrary::Abs(BoxSize.Y), Trigger->GetScaledBoxExtent().Z).ToString());
		}
	}
}

void ACheckpoint::OnOverlapBegin(class UPrimitiveComponent* OverlapComponent, class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult & SweepResult)
{
	if (ACharacter* Character = Cast<ACharacter>(OtherActor))
	{
		//Sequence 1
		FString SaveName;
		UALT_SG* LSave = UALT_FL::SGBase("SGUser", SaveName);

		//Has checkpoint been reached
		if (LSave->LevelStruct.PassedCheckpoints.Find(ID) != -1)
		{
			UE_LOG(LogCheckpoint, Log, TEXT("This checkpoint has already been reached"));
		}
		else
		{
			UE_LOG(LogCheckpoint, Log, TEXT("Checkpoint saved"));
			LSave->LevelStruct.PassedCheckpoints.AddUnique(ID);
			SaveData(LSave, SaveName);
			UGameplayStatics::SaveGameToSlot(LSave, SaveName, 0); //Is this needed?
		}

		//Sequence 2
		if (LoadLevel)
		{
			//Set Level and Load

			UE_LOG(LogCheckpoint, Log, TEXT("Loading %s"), *LevelToLoad.GetAssetName());
			UALT_FL::SetLevelAndLoad(LevelToLoad);

			//Set character transform to 0 again before switching levels

			LSave->Transform = FTransform(FRotator(0, 0, 0), FVector(0, 0, 0), FVector(1, 1, 1));
			UGameplayStatics::SaveGameToSlot(LSave, SaveName, 0);
		}

		//Print what stepped into CP
//		UE_LOG(LogCheckpoint, Log, TEXT("A character just stepped into the checkpoint"));
	}
}

void ACheckpoint::SaveData(UALT_SG* LSave, FString SaveName)
{
	//Character
	LSave->Transform; // = UGameplayStatics::GetPlayerCharacter(GetWorld(), 0)->GetActorTransform;
	LSave->Health;

	//Doors Opened
	LSave->DoorsOpened;

	//Inventory
	LSave->Gold;
//	LSave->SavedItems
	LSave->AmountOfSlots;
	LSave->Slots;
	LSave->SpawnersActivated;

	//BaseAI Stats

	//Save
	UGameplayStatics::SaveGameToSlot(LSave, SaveName, 0);
}

int ACheckpoint::GuessID()
{
	FString CurrentLevel = UGameplayStatics::GetCurrentLevelName(GetWorld());
	TArray<FString> LevelList = UALT_FL::GetAllLevelNames();
	TArray<ACheckpoint*> CheckpointsInLevel;

	//Populates an array of the checkpoints in the current level
	for (TActorIterator<ACheckpoint> ActorItr(GetWorld()); ActorItr; ++ActorItr)
	{
		CheckpointsInLevel.Add(*ActorItr);
	}

	//Ones (Based on which level we're currently on)
	int IDStart = LevelList.Find(CurrentLevel); //minus the amount of nonplayable levels

	//Tenths/Hundredths (Based on which Checkpoint this is)
	int IDEnd = CheckpointsInLevel.Find(this);

	return UALT_FL::ConcatInt(IDStart, IDEnd);
}

// Called when the game starts or when spawned
void ACheckpoint::BeginPlay()
{
	if (CenterAndFill)
	{
		int Index = 0;
		bool bCenter = true;

		//Object Axis for Loop
		for (int i = 0; i <= 3; i++)
		{
			FVector Select = i == 0 ? Trigger->GetForwardVector() * -1 : Trigger->GetForwardVector();
			Select = i == 2 ? Trigger->GetRightVector() * -1 : Select;
			Select = i == 3 ? Trigger->GetRightVector() : Select;

			FVector CurrentAxis = Select;
			FHitResult Hit;

			bool ReturnValue = GetWorld()->LineTraceSingleByObjectType(Hit, GetActorLocation(), GetActorLocation() + CurrentAxis * Distance, FCollisionObjectQueryParams(ECollisionChannel::ECC_WorldStatic));
			UKismetSystemLibrary::DrawDebugLine(GetWorld(), GetActorLocation(), GetActorLocation() + CurrentAxis * Distance, FLinearColor::Blue, 5, 5);

			//After AxisLinetrace
			if (ReturnValue)
			{
				//Distance between actor and hit
				Distances[i] = Hit.Location - Hit.TraceStart;

				if (CurrentAxis.X == -1 || CurrentAxis.Y == -1)
				{
					Locations[Index] = Hit.Location;
//					UE_LOG(LogCheckpoint, Warning, TEXT("Index %s"), *Hit.Location.ToString());
					Index = 1;
				}
			}
			else
			{
				UE_LOG(LogCheckpoint, Warning, TEXT("Checkpoint needs to have objects surrounding all sides for Fill & Center to be applied"));
				bCenter = false;
			}
		}

		//Completed
		if (bCenter)
		{
			SetActorCenter(this, Distances, Locations);
			SetBoxSize(Trigger);
		}
	}
}

void ACheckpoint::OnConstruction(const FTransform & Transform)
{
	TArray<ACheckpoint*> CheckpointsInLevel;
	TArray<FString> LevelList = UALT_FL::GetAllLevelNames();
	UWorld* Level = LevelToLoad.LoadSynchronous();

	//Populates an array of the checkpoints in the current level
	for (TActorIterator<ACheckpoint> ActorItr(GetWorld()); ActorItr; ++ActorItr)
	{
		CheckpointsInLevel.Add(*ActorItr);
	}

	/*Updates the ID for all checkpoints in the current level*/
	if (bGuessID)
	{
		for (int i = 0; i < CheckpointsInLevel.Num(); i++)
		{
			CheckpointsInLevel[i]->ID = CheckpointsInLevel[i]->GuessID();
		}
	}

	/*Checks if checkpoint has a sibling with the same ID number*/
	for (int i = 0; i < CheckpointsInLevel.Num(); i++)
	{
		if (this != CheckpointsInLevel[i] && CheckpointsInLevel[i]->ID == ID)
		{
			UE_LOG(LogCheckpoint, Warning, TEXT("Checkpoint %d has another sibling with the same ID"), ID);
		}
	}

	/*Checks if Load Level is filled out and valid*/

	//is LevelToLoad none && is LevelToLoad part of the LevelList && is LevelToLoad a Gameplay Level
	if (Level && LevelList.Find(LevelToLoad.GetAssetName()) != -1 && Cast<AALT_LSA>(Level->GetLevelScriptActor())->bGameplayLevel)
	{
		LoadLevel = true;
	}
	else
	{
		LoadLevel = false;

		if (LevelList.Find(LevelToLoad.GetAssetName()) == -1 && !LevelToLoad.GetAssetName().Equals(""))
			UE_LOG(LogCheckpoint, Warning, TEXT("%s can't be found in the level list"), *LevelToLoad.GetAssetName());
	}

	/*Draws Center & Fill distance*/
	for (int i = 0; i <= 3; i++)
	{
		FVector Select = i == 0 ? Trigger->GetForwardVector() * -1 : Trigger->GetForwardVector();
		Select = i == 2 ? Trigger->GetRightVector() * -1 : Select;
		Select = i == 3 ? Trigger->GetRightVector() : Select;

		FVector CurrentAxis = Select;

		UKismetSystemLibrary::DrawDebugLine(GetWorld(), GetActorLocation(), GetActorLocation() + CurrentAxis * Distance, FLinearColor::Blue, 0, 5);
	}
}

#pragma once

#include "UEnumLibrary.generated.h"

UENUM(BlueprintType)
enum class EItemCategories : uint8
{
	IC_Consumable 	UMETA(DisplayName = "Consumable"),
	IC_Weapon 		UMETA(DisplayName = "Weapon"),
	IC_Tool			UMETA(DisplayName = "Tool"),
	IC_Particle		UMETA(DisplayName = "Particle"),
	IC_QuestItem	UMETA(DisplayName = "Quest Item")
};

UENUM(BlueprintType)
enum class ESortType : uint8
{
	ST_Category	UMETA(DisplayName = "Category"),
	ST_Amount	UMETA(DisplayName = "Amount"),
	ST_Name		UMETA(DisplayName = "Name")
};

UENUM(BlueprintType)
enum class EStatusEffect : uint8
{
	SE_Poison UMETA(DisplayName = "Poison"),
	SE_Dark UMETA(DisplayName = "Dark"),
	SE_Gravity UMETA(DisplayName = "Gravity"),
	SE_God UMETA(DisplayName = "God"),
	SE_Haste UMETA(DisplayName = "Haste"),
	SE_Light UMETA(DisplayName = "Light"),
	SE_Speed UMETA(DisplayName = "Speed"),
	SE_Slow UMETA(DisplayName = "Slow"),
	SE_Cold UMETA(DisplayName = "Cold"),
	SE_Freezing UMETA(DisplayName = "Freezing"),
	SE_Hot UMETA(DisplayName = "Hot")
};
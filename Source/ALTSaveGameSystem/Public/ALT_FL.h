// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "ALT_SG.h"
#include "Kismet/GameplayStatics.h"
#include "Kismet/BlueprintFunctionLibrary.h"
#include "Engine/World.h"
#include "Engine/GameEngine.h"
#include "Engine/GameViewportClient.h"
#include <windows.h>
#include "PlatformFeatures.h"
#include "ALT_FL.generated.h"

/**
 * 
 */
UCLASS()
class ALTSAVEGAMESYSTEM_API UALT_FL : public UBlueprintFunctionLibrary
{
	GENERATED_BODY()
public:
	/*Returns cast of specified save game class.*/
	UFUNCTION(BlueprintCallable, meta = (Keywords = "base main save sg set", Category = "Saving"))
	static UALT_SG* SGBase(FString SaveName, FString& SaveNameReturn);

	/*Returns cast of MainMenu save*/
	UFUNCTION(BlueprintCallable, meta = (Keywords = "base main save sg set", Category = "Saving"))
		static UALT_SG* SGMBase(FString& SaveNameReturn);

	/*Returns cast of MainMenu save*/
	UFUNCTION(BlueprintPure, meta = (Keywords = "main save sg pure get", Category = "Saving"))
		static UALT_SG* SGMPure();

	/*Returns all save game file names from the Saved folder.*/
	UFUNCTION(BlueprintPure, Category = "Saving")
		static TArray<FString> GetAllSaveGameSlotNames();

	UFUNCTION(BlueprintPure, Category = "Saving")
		static TArray<FString> GetAllLevelNames();

	/*Saves a string to a file.*/
	UFUNCTION(BlueprintCallable, meta = (Keywords = "file save string", Category = "Saving"))
		static bool FileSaveString(FString SaveText, FString FileName);

	/*Returns a string from a file.*/
	UFUNCTION(BlueprintPure, meta = (Keywords = "file load string", Category = "Saving"))
		static bool FileLoadString(FString FileName, FString& SaveText);

	UFUNCTION(BlueprintCallable, meta = (Keywords = "save level load", Category = "Saving"))
		static void SetLevelAndLoad(TSoftObjectPtr<UWorld> Level);

	/*Return the concatenation of A and B*/
	UFUNCTION(BlueprintPure, meta = (DisplayName = "Append Int", Keywords = "concat int append", Category = "Math|Int"))
		static int32 ConcatInt(int32 A, int32 B);

	/* Returns the logical eXclusive NOR of two values (A XNOR B) */
	UFUNCTION(BlueprintPure, meta = (DisplayName = "XNOR Boolean", CompactNodeTitle = "XNOR", Keywords = "^ xnor"), Category = "Math|Boolean")
		static bool BooleanXNOR(bool A, bool B);

	/*Returns the OperatorToChange being the same sign as the OperatorBase*/
	UFUNCTION(BlueprintPure, meta = (DisplayName = "Make Operators The Same", CompactNodeTitle = "SameSigns", Keywords = "operator sign same change"), Category = "Math|Float")
		static float MakeOperatorsTheSame(float OperatorToChange, float OperatorBase);
};

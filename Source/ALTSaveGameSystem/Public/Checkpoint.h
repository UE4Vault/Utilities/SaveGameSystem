// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerStart.h"
#include "Kismet/KismetMathLibrary.h"
#include "Components/BoxComponent.h"
#include "Components/CapsuleComponent.h"
#include "Kismet/GameplayStatics.h"
#include "Engine/World.h"
#include "EngineUtils.h"
#include "ALTSaveGameSystem/Public/ALT_FL.h"
#include "GameFramework/Character.h"
#include "ALT_LSA.h"
#include "Checkpoint.generated.h"

DECLARE_LOG_CATEGORY_EXTERN(LogCheckpoint, Log, All);

UCLASS()
class ALTSAVEGAMESYSTEM_API ACheckpoint : public APlayerStart
{
	GENERATED_BODY()
public:
	ACheckpoint(const FObjectInitializer& ObjectInitializer);


	//Functions


	/*Sets the actor specified to the center of the ObjectAxisTraceLine result*/
	UFUNCTION(BlueprintCallable)
		void SetActorCenter(AActor* Actor, TArray<FVector> Distances, TArray<FVector> Locations);

	UFUNCTION(BlueprintCallable)
		void SetBoxSize(UBoxComponent* BoxComponent);

	UFUNCTION()
		void OnOverlapBegin(class UPrimitiveComponent* OverlapComponent, class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult & SweepResult);

	UFUNCTION()
		void SaveData(UALT_SG* LSave, FString SaveName);

	/*Guesses the correct ID*/
	UFUNCTION(BlueprintCallable)
		int GuessID();


	// Properties


	UPROPERTY(BlueprintReadOnly, Category = "Components")
		UBoxComponent* Trigger;

	/*Guesses what the most logical ID should be*/
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Checkpoint")
		bool bGuessID = true;

	/*Centers the checkpoint and then fills in the remaining space*/
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Checkpoint")
		bool CenterAndFill = false;

	/*The ID is a combination of the level number and the checkpoint number*/
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Checkpoint")
		int ID = 10;

	/*The next level to load. If none, it will be ignored*/
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Checkpoint")
		TSoftObjectPtr<UWorld> LevelToLoad;

	/*The distance that we'll search for surrounding objects to Center & Fill up to*/
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Checkpoint")
		float Distance = 3000;

	/*If true, a new level will be loaded*/
	UPROPERTY()
		bool LoadLevel;

	UPROPERTY()
		TArray<FVector>	Distances;

	UPROPERTY()
		TArray<FVector> Locations;

	UPROPERTY()
		FVector BoxSize;

	UPROPERTY()
		int NonplayableLevels;


protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:
	// Called when actor is created
	virtual void OnConstruction(const FTransform& Transform);
};
